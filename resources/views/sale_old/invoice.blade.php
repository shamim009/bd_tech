<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{url('public/logo', $general_setting->site_logo)}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Invoice</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <style type="text/css">
        * {
            font-size: 14px;
            line-height: 24px;
            font-family: 'Ubuntu', sans-serif;
            text-transform: capitalize;
        }
        .btn {
            padding: 7px 10px;
            text-decoration: none;
            border: none;
            display: block;
            text-align: center;
            margin: 7px;
            cursor:pointer;
        }
        .address{
            background-color: #281848;
            border-radius: 7px;
            color: white;
        }
        .icon{
            padding:10px;
            text-align: center;
        }
        .btn-info {
            background-color: #999;
            color: #FFF;
        }

        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }
        .company h2{
            float: right;
            font-size: 30px;
            font-family: initial;
            font-weight: bold;
            /* font-family: itallic; */
            padding: 10px 0px;
            color: #150f35;
        }
        .logo h2{
            text-align: center;
            font-weight: bold;
            font-family: fantasy;
            color: #280363;
        }
        .item-table p{
            margin:0px;
            padding:0px;
        }
        .signature{
            margin: 100px 0px;
        }
        .signature p{
            margin-bottom: 100px;
        }
        .signature h4{
            margin: 0px;
            padding: 0px;
            font-size: 16px;
            font-weight: bold;
            font-family: initial;
        }
        .app-head{
            margin: 100px 0px;
        }
        {
            .text-align: center;
        }

        .app-head p{
            margin: 0px;
            padding: 0px;
            font-family: initial;
        }
        .date p{
            text-align: right;
            margin-right: 10px;
            font-weight: bold;
            font-family: initial;
            margin-top: 120px;
        }
        .centered {
            text-align: center;
            align-content: center;
        }
        small{font-size:11px;}

        @media print {
            * {
                font-size:12px;
                line-height: 20px;
            }

            .hidden-print {
                display: none !important;
            }
            @page { margin: 0; } body { margin: 0.5cm; margin-bottom:1.6cm; }
        }
    </style>
  </head>
<body>

<div class="container">
<div>
    @if(preg_match('~[0-9]~', url()->previous()))
        @php $url = '../../pos'; @endphp
    @else
        @php $url = url()->previous(); @endphp
    @endif
    <div class="hidden-print">
        <table>
            <tr>
                <td><a href="{{$url}}" class="btn btn-info"><i class="fa fa-arrow-left"></i> {{trans('file.Back')}}</a> </td>
                <td><button onclick="window.print();" class="btn btn-primary"><i class="dripicons-print"></i> {{trans('file.Print')}}</button></td>
            </tr>
        </table>
        <br>
    </div>

    <div id="receipt-data">
         <div class="row">
             <div class="col-md-12" style="margin-top:50px;">
                 <div class="logo">
                     <h2>Uni Smart Healthcare Ltd</h2>
                     <div class="icon">
                        <i class="fa fa-home"> 1197 (4th Floor), Avenue-10, DOHS, Mirpur, Dhaka-1216. </i><br>
                        <i class="fa fa-envelope"> info@unismartbd.com </i><br>
                        <i class="fa fa-phone"> +88-02-58071124, +8801680 805 480 </i><br>
                        <i class="fa fa-globe" aria-hidden="true"> www.unismartbd.com </i><br>
                    </div>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="app-head">
                    <p>To</p>
                    <p>{{ $lims_sale_data->customer->name }}</p>
                    <p>Address: {{ $lims_sale_data->customer->address }}, City: {{ $lims_sale_data->customer->city }}</p>
                    <p>{{ $lims_sale_data->customer->country }}</p>
                    <p>Email: {{ $lims_sale_data->customer->email }}, Phone: {{ $lims_sale_data->customer->phone_number }}</p>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="date">
                    <p>Date: {{ Carbon\Carbon::now()->format('d F, Y')}}</p>
                 </div>
            </div>
            <div class="col-md-6">

            </div>
            <div class="col-md-12">
                <div class="item-table">
                    <table class="table table-hover table-striped" style="border: 1px solid #dcd9d9;">
                         <thead>
                             <tr>
                                 <th width="10%">SL NO</th>
                                 <th width="30%">Description</th>
                                 <th width="20%" style="text-align:center;">Quantity</th>
                                 <th width="20%" style="text-align:center;">Unit Price</th>
                                 <th width="20%" style="text-align:center;">Amount</th>
                             </tr>
                         </thead>
                         <tbody>
                              @php
                                 $sl =0;$product_qty=0;$product_unit=0;$total_price=0;$service_qty=0;$service_unit=0;$total_price_ser=0;
                             @endphp
                            @foreach ($lims_product_sale_data as $key => $product)
                                <?php
                                    $lims_product_data = \App\Product::find($product->product_id);
                                    if($product->variant_id) {
                                        $variant_data = \App\Variant::find($product->variant_id);
                                        $product_name = $lims_product_data->name.' ['.$variant_data->name.']';
                                    }
                                    else
                                        $product_name = $lims_product_data->name.' ['.$lims_product_data->code.']';
                                ?>
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>
                                        <p>Item Name: {{ $product_name }}</p>
                                        <p>Category: {{ $product->product->category->name }}</p>
                                        @if(isset($product->product->brand))
                                             <p>Brand: {{ $product->product->brand->title }}</p>
                                        @endif
                                    </td>
                                    <td style="text-align:center;">{{ $product->qty }}</td>
                                    @php
                                        $product_qty +=$product->qty;
                                    @endphp
                                    <td style="text-align:center;">{{number_format((float)($product->total / $product->qty), 2, '.', '')}}</td>
                                    @php
                                        $product_unit += ($product->total / $product->qty);
                                    @endphp
                                    <td style="text-align:center;">{{number_format((float)$product->total, 2, '.', '')}}</td>
                                    @php
                                        $total_price += $product->total;
                                    @endphp
                                    @php
                                        $sl += 1;
                                    @endphp
                                </tr>
                            @endforeach
                            @foreach ($lims_service_sale_data as $key => $service)
                                <?php
                                    $lims_service_data = \App\Service::find($service->product_id);
                                    $product_name = $lims_service_data->name.' ['.$lims_service_data->code.']';
                                ?>
                                <tr>
                                    <td>{{ $sl+1 }}</td>
                                    <td>
                                        <p>Item Name: {{ $product_name }}</p>
                                        <p>Category: {{ $service->product->category->name }}</p>
                                        {!! $service->product->details !!}
                                    </td>
                                    <td style="text-align:center;">{{ $service->qty }}</td>
                                    @php
                                        $service_qty +=$service->qty;
                                    @endphp
                                    <td style="text-align:center;">{{number_format((float)($service->total / $product->qty), 2, '.', '')}}</td>
                                    @php
                                        $service_unit += ($service->total / $product->qty);
                                    @endphp
                                    <td style="text-align:center;">{{number_format((float)$service->total, 2, '.', '')}}</td>
                                    @php
                                        $total_price_ser += $service->total;
                                    @endphp
                                </tr>
                            @endforeach
                         </tbody>
                         <tfoot>
                             <tr>
                                <th width="10%"></th>
                                <th width="30%">Total</th>
                                <th width="20%" style="text-align:center;">{{$lims_sale_data->total_qty}}</th>
                                <th width="20%" style="text-align:center;">{{number_format((float)$service_unit + $product_unit, 2, '.', '')}}</th>
                                <th width="20%" style="text-align:center;">{{number_format((float)$total_price_ser + $total_price, 2, '.', '')}}</th>
                             </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="signature">
                     <p>Best Regards</p>
                     <h4>Managing Director</h4>
                     <h4>UniSmart HealthCare Ltd</h4>
                     <h4>Mobile: +8801680605460</h4>
                 </div>
            </div>
         </div>
         <div class="row" style="margin:100px 0px;">
            <div class="col-md-6" style="margin:0px;padding:0px">
                <div class="address">
                    <div class="icon">
                        <i class="fa fa-home"> 1197 (4th Floor), Avenue-10, DOHS, Mirpur, Dhaka-1216. </i>
                        <i class="fa fa-envelope"> info@unismartbd.com </i>
                        <i class="fa fa-phone"> +88-02-58071124, +8801680 805 480 </i>
                        <i class="fa fa-globe" aria-hidden="true"> www.unismartbd.com </i>
                    </div>
                </div>
             </div>
             <div class="col-md-6">
                 <div class="company">
                     <h2>Uni Smart Healthcate Ltd.</h2>
                 </div>
             </div>
         </div>
    </div>
</div>
</div>
<script type="text/javascript">
    function auto_print() {
        window.print()
    }
    setTimeout(auto_print, 1000);
</script>

</body>
</html>
