<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{url('public/logo', $general_setting->site_logo)}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Delivary Challan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <style type="text/css">
        * {
            font-size: 16px;
            line-height: 24px;
            font-family: 'Ubuntu', sans-serif;
            text-transform: capitalize;
        }

        .page{
            background-image:url({{url('public/logo/center-logo.jpg')}});
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 1000px 800px;
        }
        .btn {
            padding: 7px 10px;
            text-decoration: none;
            border: none;
            display: block;
            text-align: center;
            margin: 7px;
            cursor:pointer;
        }
        .address{
            background-color: #281848;
            border-radius: 7px;
            color: white;
        }
        .custom_table td{
            padding: .75rem !important;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        .custom_table th{
            padding: .75rem !important;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        .icon{
            padding:10px;
            text-align: center;
            line-height: 16px;
        }
        .btn-info {
            background-color: #999;
            color: #FFF;
        }

        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }
        .company h2{
            float: right;
            font-size: 30px;
            font-family: initial;
            font-weight: bold;
            /* font-family: itallic; */
            padding: 10px 0px;
            color: #150f35;
        }
        .logo h2{
            text-align: center;
            font-weight: bold;
            font-family: fantasy;
            color: #280363;
        }
        .item-table p{
            margin:0px;
            padding:0px;
            line-height: 15px;
            font-size: 14px;
        }
        .signature{
            
        }
        .signature p{
            margin-bottom: 120px;
        }
        .signature h4{
            margin: 0px;
            padding: 0px;
            font-size: 16px;
            font-weight: bold;
            font-family: initial;
        }
        .app-head{

        }
        .app-head p{
            margin: 0px;
            padding: 0px;
            font-family: initial;
            line-height: 16px;
        }
        .date p{
            text-align: right;
            font-weight: bold;
            font-family: initial;
            margin-bottom: 0px;
            line-height: 15px;
        }
        .centered {
            text-align: center;
            align-content: center;
        }
        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }
        small{font-size:11px;}

        .page-header {
             height: 200px;
        }
        .invoice_title{
            border: 1px solid #2d0080;
            margin: 0px 360px;
            font-size: 16px;
            font-weight: bold;
            text-align: center;
            padding: 5px 10px;
        }
        .page-header-space {
            height: 200px;
        }

        .page-footer, .page-footer-space {
            height: 250px;
        }
        .page-footer {
            position: fixed;
            bottom: 0;
            height: 250px;
        }
        .page-header {
            position: fixed;
            width: 90%;
        }
        .page {
            page-break-after: always;
        }

    @media print {
        *{
            font-size:14px;
            line-height: 20px;
        }
        .page{
            background-image:url({{url('public/logo/center-logo.jpg')}});
            background-repeat:no-repeat;
            background-position: center center;
            background-size: 1000px 800px;
        }
        .invoice_title{
            border: 1px solid #2d0080;
            margin: 0px 360px;
            font-size: 16px;
            font-weight: bold;
            text-align: center;
            padding: 5px 10px;
        }
        .hidden-print {
            display: none !important;
        }
        .page-header {
            position: fixed;
            width: 90%;
            height: 100px;
        }
        .page-footer {
            position: fixed;
            bottom: 0;
            height: 250px;
        }
        thead {display: table-header-group;}
        tfoot {display: table-footer-group;}
        button {display: none;}
        body {margin: 0;}

        body { -webkit-print-color-adjust: exact !important;}
    }
    </style>
  </head>
<body>

<div class="container">
<div>
    @if(preg_match('~[0-9]~', url()->previous()))
        @php $url = '../'; @endphp
    @else
        @php $url = url()->previous(); @endphp
    @endif
    <div class="hidden-print">
        <table>
            <tr>
                <td><a href="{{$url}}" class="btn btn-info"><i class="fa fa-arrow-left"></i> {{trans('file.Back')}}</a> </td>
                <td><button onclick="window.print();" class="btn btn-primary"><i class="dripicons-print"></i> {{trans('file.Print')}}</button></td>
            </tr>
        </table>
        <br>
    </div>

    <div id="receipt-data">
        <div class="row">
            <div class="col-md-12">
                <div class="" style="border:none;">
                    <div class="card-body">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card" style="border:none;">
                                        <div class="card-body" style="padding:0px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="logo">
                                                        <div class="icon">
                                                            <img src="{{url('public/logo', $general_setting->site_logo)}}" alt="Side Image"></br>
                                                            <i class="fa fa-home"> 1197 (4th Floor), Avenue-10, DOHS, Mirpur, Dhaka-1216. </i><br>
                                                            <i class="fa fa-envelope" style="text-transform: lowercase;"> info@unismartbd.com </i><br>
                                                            <i class="fa fa-phone"> +8801688 005 882, +8801680 605 460 </i><br>
                                                            <i class="fa fa-globe" aria-hidden="true" style="text-transform: lowercase;"> www.unismartbd.com </i><br>
                                                        </div>
                                                    </div>
                                                </div>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="page" style="margin:0px 0px;">
                        <table width="100%">
                            <thead>
                                <tr>
                                  <td>
                                    <!--place holder for the fixed-position header-->
                                    <div class="page-header-space"></div>
                                  </td>
                                </tr>
                              </thead>

                            <tbody>
                                <td>
                                    <div class="row" style="margin-top:30px;">
                                        <div class="col-md-12">
                                             <h2 class="invoice_title">Delivery Challan</h2>
                                        </div>
                                        <div class="col-md-6 app-head">
                                               <p>To</p>
                                               <p>{{ $lims_sale_data->customer->name }}</p>
                                               <p><b>{{ $lims_sale_data->customer->company_name }}</b></p>
                                               <p>Address: {{ $lims_sale_data->customer->address }}, City: {{ $lims_sale_data->customer->city }}</p>
                                               <p>{{ $lims_sale_data->customer->country }}</p>
                                               <p style="text-transform: lowercase;">Email: {{ $lims_sale_data->customer->email }}, Phone: {{ $lims_sale_data->customer->phone_number }}</p>
                                        </div>
                                        <div class="col-md-6 date">
                                                <p>Date: {{ date('d F, Y', strtotime($lims_sale_data->date))}}</p>
                                                <p>Invoice No: {{$lims_sale_data->invoice_no }}</p>
                                        </div>

                                        <div class="col-md-12" style="margin-top: 30px;">
                                            <div class="item-table">
                                                <table class="custom_table" style="border: 1px solid #dcd9d9;" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%">SL NO</th>
                                                            <th width="50%">Description</th>
                                                            <th width="30%" style="text-align:center;">Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         @php
                                                            $sl =0;
                                                        @endphp
                                                       @foreach ($lims_product_sale_data as $key => $product)
                                                           <?php
                                                               $lims_product_data = \App\Product::find($product->product_id);
                                                               if($product->variant_id) {
                                                                   $variant_data = \App\Variant::find($product->variant_id);
                                                                   $product_name = $lims_product_data->name.' ['.$variant_data->name.']';
                                                               }
                                                               else
                                                                   $product_name = $lims_product_data->name.' ['.$lims_product_data->code.']';
                                                           ?>
                                                           <tr>
                                                               <td>{{ $key+1 }}</td>
                                                               <td>
                                                                   <p>Item Name: {{ $product_name }}</p>
                                                                   <p>Category: {{ $product->product->category->name }}</p>
                                                                   @if(isset($product->product->brand))
                                                                        <p>Brand: {{ $product->product->brand->title }}</p>
                                                                   @endif
                                                                   @if(isset($product->product->model))
                                                                        <p>Model: {{ $product->product->model->model }}, 
                                                                             @if(isset($product->product->origin))
                                                                                Origin: {{ $product->product->origin->origin }}
                                                                             @endif
                                                                        </p>
                                                                   @endif
                                                               </td>
                                                               <td style="text-align:center;">{{ $product->qty }}</td>
                                                               @php
                                                                   $sl += 1;
                                                               @endphp
                                                           </tr>
                                                       @endforeach
                                                       @foreach ($lims_service_sale_data as $key => $service)
                                                           <?php
                                                               $lims_service_data = \App\Service::find($service->product_id);
                                                               $product_name = $lims_service_data->name.' ['.$lims_service_data->code.']';
                                                           ?>
                                                           <tr>
                                                               <td>{{ $sl+1 }}</td>
                                                               <td>
                                                                   <p>Item Name: {{ $product_name }}</p>
                                                                   @if ($service->product->category)
                                                                   <p>Category: {{ $service->product->category->name }}</p>
                                                                   @endif
                                                                   {!! $service->product->details !!}
                                                               </td>
                                                               <td style="text-align:center;">{{ $service->qty }}</td>
                                                           </tr>
                                                       @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                            </tbody>

                            <tfoot>
                              <tr>
                                <td>
                                  <div class="page-footer-space"></div>
                                </td>
                              </tr>
                            </tfoot>
                        </table>
                    </div><!--end page-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
<div class="page-footer" style="margin-bottom:20px;">
    <div class="row">
        <div class="container">
        <div class="col-md-12" margin="5px 10px;">
            <table width="100%" style="color: #080c8f;
            font-weight: bold;">
                <tbody>
                     <tr>
                        <td>
                            <div class="signature">
                                <p>Best Regards</p>
                                <h4>Store & Accounts</h4>
                                <h4>Uni Smart Healthcare Ltd</h4>
                                <h4>Mobile: +8801688005882</h4>
                            </div>
                        </td>
                     </tr>
                </tbody>
            </table>
        </div>
        </div>
        <div class="col-md-12" style="padding:0px;">
            <div class="logo_foot">
                <img src="{{url('public/logo/footer-bg.jpg')}}" alt="Side Image" style="width:100%;">
          </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function auto_print() {
        window.print()
    }
    setTimeout(auto_print, 1000);
</script>

</body>
</html>
