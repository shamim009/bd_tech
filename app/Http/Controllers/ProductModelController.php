<?php

namespace App\Http\Controllers;

use App\ProductModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ProductModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model_all = ProductModel::all();
        return view('product_model.create',compact('model_all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'model' => 'string|required',
        ]);
        $input = $request->all();
        ProductModel::create($input);

        return redirect('models')->with('message','Product model added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function show(ProductModel $productModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ProductModel::find($id);
        return $model;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'model' => 'string|required',
        ]);
        $input = $request->all();
        $model = ProductModel::find($request->model_id);
        $model->update($input);

        return redirect('models')->with('message','Product model updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductModel::find($id)->delete();
        return redirect()->back()->with('message','Product model deleted successfully');
    }
}
