<?php

namespace App\Http\Controllers;

use App\ProductOrigin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductOriginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $origin_all = ProductOrigin::all();
        return view('product_origin.create',compact('origin_all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'origin' => 'string|required',
        ]);
        $input = $request->all();
        ProductOrigin::create($input);

        return redirect('origins')->with('message','Product origin added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductOrigin  $productOrigin
     * @return \Illuminate\Http\Response
     */
    public function show(ProductOrigin $productOrigin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductOrigin  $productOrigin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $origin = ProductOrigin::find($id);
        return $origin;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductOrigin  $productOrigin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'origin' => 'string|required',
        ]);
        $input = $request->all();
        $origin = ProductOrigin::find($request->origin_id);
        $origin->update($input);

        return redirect('origins')->with('message','Product origin updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductOrigin  $productOrigin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductOrigin::find($id)->delete();
        return redirect()->back()->with('message','Product origin deleted successfully');
    }
}
