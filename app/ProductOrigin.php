<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrigin extends Model
{
    protected $fillable = [
        "origin"
    ];
}
